package com.example.mcdonut.model;

import com.example.mcdonut.model.database.Populate;

public class Product {

    static{
        Populate.obtainProducts();
    }

    private int idProduct;
    private String details, title, type;

    public Product(){
    }

    public Product(int idProduct) {
        this.idProduct = idProduct;
    }

    public Product(int idProduct, String details, String title, String type) {
        this.idProduct = idProduct;
        this.details = details;
        this.title = title;
        this.type = type;
    }

    public int getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(int idProduct) {
        this.idProduct = idProduct;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Product{" +
                "idProduct=" + idProduct +
                ", details='" + details + '\'' +
                ", title='" + title + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
