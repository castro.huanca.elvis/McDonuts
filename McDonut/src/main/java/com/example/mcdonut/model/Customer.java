package com.example.mcdonut.model;

import com.example.mcdonut.model.database.Populate;

public class Customer {

    static {
        Populate.obtainCustomers();
    }

    private String fullName, password, email, dateOfBirth;
    private int id, nit;

    public Customer(){

    }

    public Customer(String fullName, String password, String email, String dateOfBirth, int id, int nit) {
        this.fullName = fullName;
        this.password = password;
        this.email = email;
        this.dateOfBirth = dateOfBirth;
        this.id = id;
        this.nit = nit;
    }

    public Customer(int id) {
        this.id = id;
    }

    public Customer(String password, String email) {
        this.password = password;
        this.email = email;
    }


    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public int getNit() {
        return nit;
    }

    public void setNit(int nit) {
        this.nit = nit;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "fullName='" + fullName + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", dateOfBirth='" + dateOfBirth + '\'' +
                ", id=" + id +
                ", nit=" + nit +
                '}';
    }

    public static void main(String[] args) {
        Customer c = new Customer();
        Product p = new Product();
    }
}
