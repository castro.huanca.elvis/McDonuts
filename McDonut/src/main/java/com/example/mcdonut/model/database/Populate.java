package com.example.mcdonut.model.database;

import com.example.mcdonut.model.*;
import java.sql.*;
import java.util.ArrayList;

import static com.example.mcdonut.model.database.DataBaseConnection.*;

public class Populate {

    public static ArrayList<Customer> customers = new ArrayList<>();
    public static ArrayList<Product> products = new ArrayList<>();

    public static ArrayList<Customer> obtainCustomers(){
        String sql = "SELECT id_client, nit_client, full_name, date_of_birth, password_customer, email FROM Customer";
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet result = null;

        try {
            conn = DataBaseConnection.getConnection();
            stmt = conn.prepareStatement(sql);
            result = stmt.executeQuery();

            while ( result.next() ){
                int idCustomer = result.getInt("id_client");
                int nitCustomer = result.getInt("nit_client");
                String fullName = result.getString("full_name");
                String dateOfBirth = result.getString("date_of_birth");
                String password_customer = result.getString("password_customer");
                String email = result.getString("email");

                customers.add(new Customer(fullName, password_customer, email, dateOfBirth, idCustomer, nitCustomer));

            }

        } catch (SQLException e) {
            e.printStackTrace(System.out);
        }
        finally {
            try {
                close(result);
                close(stmt);
                close(conn);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return customers;
    }




    public static ArrayList<Product> obtainProducts(){
        String sql = "SELECT id_product, details, title, type_product FROM Product";

        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet result = null;

        try {
            conn = DataBaseConnection.getConnection();
            stmt = conn.prepareStatement(sql);
            result = stmt.executeQuery();

            while ( result.next() ){
                int idProduct = result.getInt("id_product");
                String details = result.getString("details");
                String title = result.getString("title");
                String typeProduct = result.getString("type_product");

                products.add(new Product(idProduct, details, title, typeProduct));
            }
        } catch (SQLException e) {
            e.printStackTrace(System.out);
        }
        finally {
            try {
                close(result);
                close(stmt);
                close(conn);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return products;
    }

}
