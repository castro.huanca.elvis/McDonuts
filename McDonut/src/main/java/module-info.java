module com.example.mcdonut {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;


    opens com.example.mcdonut to javafx.fxml;
    exports com.example.mcdonut;
}